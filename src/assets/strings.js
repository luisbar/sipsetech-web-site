// Index
const TXT_1 = [
  {
    label: 'Inicio',
    componentToShow: 'home',
  },
  {
    label: 'Nosotros',
    componentToShow: 'about',
  },
  {
    label: 'Productos',
    componentToShow: 'products',
  },
  {
    label: 'Contacto',
    componentToShow: 'contact',
  },
];
//Home
const TXT_2 = 'Soluciones IP en Seguridad Tecnológica';
const TXT_3 = 'Productos';
const TXT_4 = 'vmescobar@sipsetech.com';
const TXT_5 = [
  'https://twitter.com/luisbar180492',
  'https://www.linkedin.com/in/luis-angel-barrancos-ortiz-b76b70169/',
  'https://www.facebook.com/luisangel.barrancosortiz',
  'whatsapp://send?abid=79071059',
];
//About
const TXT_6 = [
  {
    id: 'luis',
    title: 'Ing. Luis Pablo Prat Serrano',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
  },
  {
    id: 'vistor',
    title: 'Ing. Victor Manuel Escobar',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
  },
];
const TXT_7 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et molestie sem, in porta felis. Integer mauris tortor, tincidunt ac ultricies vitae, elementum sit amet augue. Fusce egestas sagittis sapien ut rhoncus. Nulla id orci egestas, faucibus leo at, aliquet justo.';
const TXT_8 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et molestie sem, in porta felis. Integer mauris tortor, tincidunt ac ultricies vitae, elementum sit amet augue. Fusce egestas sagittis sapien ut rhoncus. Nulla id orci egestas, faucibus leo at, aliquet justo.';
//Products
const TXT_9 = [
  {
    name: 'protect',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et molestie sem, in porta felis. Integer mauris tortor, tincidunt ac ultricies vitae, elementum sit amet augue. Fusce egestas sagittis sapien ut rhoncus. Nulla id orci egestas, faucibus leo at, aliquet justo.',
    link: 'https://protectglobal.com/',
  },
  {
    name: 'mobotix',
    description: 'mobotix description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et molestie sem, in porta felis. Integer mauris tortor, tincidunt ac ultricies vitae, elementum sit amet augue. Fusce egestas sagittis sapien ut rhoncus. Nulla id orci egestas, faucibus leo at, aliquet justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et molestie sem, in porta felis. Integer mauris tortor, tincidunt ac ultricies vitae, elementum sit amet augue. Fusce egestas sagittis sapien ut rhoncus. Nulla id orci egestas, faucibus leo at, aliquet justo.',
    link: 'https://www.mobotix.com/',
  },
  {
    name: 'paxton',
    description: 'paxton description',
    link: 'https://securitytechnologyofsouthtexas.com/security-technology-of-south-texas-inc-attended-the-paxton-certification/',
  },
  {
    name: 'axis',
    description: 'axis description',
    link: 'https://www.axis.com/es',
  },
  {
    name: 'iss',
    description: 'iss description',
    link: 'https://www.issivs.com/',
  },
  {
    name: 'ubiquiti',
    description: 'ubiquiti description',
    link: 'https://www.ui.com/',
  },
];
//Contact
const TXT_10 = [
  {
    id: 'name',
    label: 'Nombre',
    type: 'text',
  },
  {
    id: 'email',
    label: 'Correo',
    type: 'text',
  },
  {
    id: 'subject',
    label: 'Asunto',
    type: 'text',
  },
  {
    id: 'message',
    label: 'Mensaje',
    type: 'textarea',
  },
];

export {
  TXT_1,
  TXT_2,
  TXT_3,
  TXT_4,
  TXT_5,
  TXT_6,
  TXT_7,
  TXT_8,
  TXT_9,
  TXT_10,
};