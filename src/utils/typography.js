import Typography from 'typography';
const theme = {
  title: 'museo',
  baseFontSize: '16px',
  baseLineHeight: 1.5,// Distance between lines
  scaleRatio: 2,// Size of hs and is calculated from baseFontSize
  googleFonts: [// Fonst fetched from google fonts
    {
      name: 'Montserrat',
      styles: [
        'regular',
      ],
    },
  ],
  headerFontFamily: ['Museo Sans', 'Monserrat'],// Fonts for hs, we need at least two
  bodyFontFamily: ['Museo Sans', 'Monserrat'],// Fonts for p, span, and div, we need at least two
  headerWeight: 'bold',// Weight of hs
  bodyWeight: 'normal',// Weigut of p, span and div
  blockMarginBottom: 1,// Hs bottom margin
  includeNormalize: true,// Include normalize.css
  bodyColor: 'hsl(0, 0%, 50%)',// Color of p, span and div
  headerColor: 'hsl(0, 0%, 50%)',// Hs color
};

const typography = new Typography(theme);

export const { scale, rhythm, options } = typography
export default typography