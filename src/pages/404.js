import React from 'react';

import Seo from '../components/atoms/Seo';

const NotFoundPage = () => (
  <>
    <Seo title='404: No Encontrado' />
    <h1>No Encontrado</h1>
    <p>El recurso no existe</p>
  </>
)

export default NotFoundPage;
