import React from 'react';
import { graphql } from 'gatsby';

import Header from '../components/molecules/Header';
import Home from '../components/organisms/Home';
import About from '../components/organisms/About';
import Products from '../components/organisms/Products';
import Contact from '../components/organisms/Contact';
import { TXT_1, TXT_2, TXT_3, TXT_4, TXT_5, TXT_6, TXT_7, TXT_8, TXT_9, TXT_10 } from '../assets/strings';
import styles from '../styles/pages/index.module.scss';

const IndexPage = ({ data: { desktopLogo, mobileLogo, memberImages, productImages } }) => {
  
  return (
    <>
      <img
        className={styles.img}
        src={require('../assets/images/sipsetech-rotated.svg')}
      />
      <Header
        desktopLogo={desktopLogo.childImageSharp.fixed}
        mobileLogo={mobileLogo.childImageSharp.fixed}
        menuItems={TXT_1}
      />
      <Home
        id={TXT_1[0].componentToShow}
        description={TXT_2}
        viewId={TXT_1[2].componentToShow}
        buttonLabel={TXT_3}
        mail={TXT_4}
        socialNetworks={TXT_5}
      />
      <About
        id={TXT_1[1].componentToShow}
        name={TXT_1[1].label}
        mision={TXT_7}
        vision={TXT_8}
        members={TXT_6.map((member) => {
          const imageIndex = memberImages.edges.findIndex((edge) => edge.node.fluid.src.indexOf(member.id) !== -1)

          return {
            ...member,
            image: memberImages.edges[imageIndex].node.fluid
          }
        })}
      />
      <Products
        id={TXT_1[2].componentToShow}
        name={TXT_1[2].label}
        products={TXT_9.map((product) => {
          const imageIndex = productImages.edges.findIndex((edge) => edge.node.fixed.src.indexOf(product.name) !== -1)
          
          return {
            ...product,
            image: productImages.edges[imageIndex].node.fixed
          }
        })}
      />
      <Contact
        id={TXT_1[3].componentToShow}
        name={TXT_1[3].label}
        inputs={TXT_10}
      />
    </>
  );
};

export const query = graphql`
  query {
    desktopLogo: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        fixed(height: 150, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    mobileLogo: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        fixed(height: 110, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    memberImages: allImageSharp {
      edges {
        node {
          fluid(quality: 10) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
    productImages: allImageSharp {
      edges {
        node {
          fixed(height: 150, quality: 100) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  }
`

export default IndexPage
