import React from 'react';

import Map from '../molecules/Map';
import Form from '../molecules/Form';
import styles from '../../styles/organisms/mapAndForm.module.scss';

const MapAndForm = ({ inputs }) => {
  return (
    <div
      className={styles.container}
    >
      <Map />
      <Form
        inputs={inputs}
      />
    </div>
  );
};

export default MapAndForm;