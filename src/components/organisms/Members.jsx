import React from 'react';

import Member from '../molecules/Member';
import styles from '../../styles/organisms/members.module.scss';

const Members = ({ members }) => {
  return (
    <div
      className={styles.container}
    >
      {
        members.map((member) => {
          return (
            <Member
              key={member.title}
              fluidImage={member.image}
              title={member.title}
              description={member.description}
            />
          );
        })
      }
    </div>
  );
};

export default Members;