import React from 'react';

import AboutProductsAndContactTemplate from '../templates/AboutProductsAndContactTemplate';
import Title from '../atoms/Title';
import Members from './Members';
import MisionAndVision from './MisionAndVision'; 

const About = ({ id, name, members, mision, vision }) => {
  return (
    <AboutProductsAndContactTemplate
      id={id}
      header={
        <Title
          text={name}
        />
      }
      main={
        <>
          <Members
            members={members}
          />
          <MisionAndVision
            mision={mision}
            vision={vision}
          />
        </>
      }
    />
  );
};

export default About;