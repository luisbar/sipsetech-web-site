import React from 'react';

import AboutProductsAndContactTemplate from '../templates/AboutProductsAndContactTemplate';
import Title from '../atoms/Title';

import MapAndForm from './MapAndForm';

const Contact = ({ id, name, inputs }) => {
  return (
    <AboutProductsAndContactTemplate
      id={id}
      header={
        <Title
          text={name}
        />
      }
      main={
        <MapAndForm
          inputs={inputs}
        />
      }
    />
  );
};

export default Contact;