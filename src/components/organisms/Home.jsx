import React from 'react';

import HomeTemplate from '../templates/HomeTemplate';
import ActionCall from '../molecules/ActionCall';
import HomeFooter from '../molecules/HomeFooter';

const Home = ({ id, description, buttonLabel, viewId, socialNetworks, mail }) => {
  return (
    <HomeTemplate
      id={id}
      main={
        <ActionCall
          description={description}
          buttonLabel={buttonLabel}
          buttonAction={() => window.location.href = `#${viewId}` }
        />
      }
      footer={
        <HomeFooter
          socialNetworks={socialNetworks}
          mail={mail}
        />
      }
    />
  );
};

export default Home;