import React from 'react';
import PropTypes from 'prop-types';

import ProductCard from '../molecules/ProductCard';
import styles from '../../styles/organisms/productCards.module.scss';

const ProductCards = ({ products }) => {
  return (
    <div
      className={styles.container}
    >
      {
        products.map(({ image, description, link }) => {
          return (
            <ProductCard
              key={link}
              image={image}
              description={description}
              link={link}
            />
          );
        })
      }
    </div>
  );
};

ProductCards.propTypes = {
  products: PropTypes.array,
};

ProductCards.defaultProps = {
  products: [],
};

export default ProductCards;