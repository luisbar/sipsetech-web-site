import React from 'react';

import AboutProductsAndContactTemplate from '../templates/AboutProductsAndContactTemplate';
import Title from '../atoms/Title';
import ProductCards from './ProductCards';

const Products = ({ id, name, products }) => {
  return (
    <AboutProductsAndContactTemplate
      id={id}
      header={
        <Title
          text={name}
        />
      }
      main={
        <ProductCards
          products={products}
        />
      }
    />
  );
};

export default Products;