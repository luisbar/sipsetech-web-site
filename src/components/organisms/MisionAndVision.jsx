import React from 'react';

import Paragraph from '../molecules/Paragraph';
import styles from '../../styles/organisms/misionAndVision.module.scss';

const MisionAndVision = ({ mision, vision }) => {
  return (
    <div
      className={styles.container}
    >
      <Paragraph
        title={'Misión'}
        description={mision}
      />
      <Paragraph
        title={'Visión'}
        description={vision}
      />
    </div>
  );
};

export default MisionAndVision;