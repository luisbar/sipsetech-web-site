import React, { useState } from 'react';
import Img from 'gatsby-image';
import PropTypes from 'prop-types';

import styles from '../../styles/molecules/header.module.scss';
import MenuButton from '../atoms/MenuButton';
import UnresponsiveMenu from './UnresponsiveMenu';
import ResponsiveMenu from './ResponsiveMenu';

const Header = ({ desktopLogo, mobileLogo, menuItems }) => {
  const [responsiveMenuVisibility, setResponsiveMenuVisibility] = useState(false);

  const onMenuButtonClicked = () => {
    setResponsiveMenuVisibility(!responsiveMenuVisibility);
  };

  const onMenuItemClicked = () => {
    setResponsiveMenuVisibility(false);
  }

  return (
    <header
      className={styles.header}
    >
      <div
        className={styles.imgContainer}
      >
        <MenuButton
          onMenuButtonClicked={onMenuButtonClicked}
          buttonWasPressed={responsiveMenuVisibility}
        />
        <Img
          className={styles.desktopImg}
          fixed={desktopLogo}
        />
        <Img
          className={styles.mobileImg}
          fixed={mobileLogo}
        />
      </div>
      <UnresponsiveMenu
        menuItems={menuItems}
      />
      <ResponsiveMenu
        menuItems={menuItems}
        visibility={responsiveMenuVisibility}
        onMenuItemClicked={onMenuItemClicked}
      />
    </header>
  );
}

Header.propTypes = {
  desktopLogo: PropTypes.object.isRequired,
  mobileLogo: PropTypes.object.isRequired,
  menuItems: PropTypes.array,
};

Header.defaultProps = {
};

export default Header;
