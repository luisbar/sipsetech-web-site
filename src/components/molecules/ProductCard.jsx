import React from 'react';
import PropTypes from 'prop-types';

import PlainButton from '../atoms/PlainButton';
import styles from '../../styles/molecules/productCard.module.scss';

const ProductCard = ({ image, description, link }) => {

  const onClick = () => {
    window.open(link);
  }

  return (
    <div
      className={styles.container}
    >
      <img
        className={styles.image}
        src={image.src}
      />
      <p
        className={styles.description}
      >
        {description}
      </p>
      <div
        className={styles.link}
      >
        <PlainButton
          label={'Ir al sitio'}
          onClick={onClick}
        />
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  image: PropTypes.object.isRequired,
  description: PropTypes.string,
  link: PropTypes.string,
};

ProductCard.defaultProps = {
  description: 'Your description',
  link: 'https://facebook.com',
};

export default ProductCard;