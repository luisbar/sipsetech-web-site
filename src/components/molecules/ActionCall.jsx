import React from 'react';
import PropTypes from 'prop-types';

import VibratorButton from '../atoms/VibratorButton';
import styles from '../../styles/molecules/actionCall.module.scss';

const ActionCall = ({ description, buttonLabel, buttonAction }) => {
  return (
    <>
      <p
        className={styles.p}
      >
        {description}
      </p>
      <VibratorButton
        label={buttonLabel}
        onClick={buttonAction}
      />
    </>
  );
};

ActionCall.propTypes = {
  description: PropTypes.string,
  buttonLabel: PropTypes.string,
  buttonAction: PropTypes.func.isRequired,
};

ActionCall.defaultProps = {
  description: 'Some clear data about your business',
  buttonLabel: 'Important Section'
};

export default ActionCall;