import React from 'react';

import Subtitle from '../atoms/Subtitle';
import styles from '../../styles/molecules/paragraph.module.scss';

const Paragraph = ({ title, description }) => {
  return (
    <div
      className={styles.container}
    >
      <img
        className={styles.img}
        src={require('../../assets/images/sipsetech-top.svg')}
      />
      <div
        className={styles.subtitle}
      >
        <Subtitle
          text={title}
        />
      </div>
      <div
        className={styles.description}
      >
        <span>
          {description}
        </span>
      </div>
    </div>
  );
};

export default Paragraph;