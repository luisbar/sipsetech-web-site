import React from 'react';
import PropTypes from 'prop-types';

import PlainButton from '../atoms/PlainButton';
import styles from '../../styles/molecules/unresponsiveMenu.module.scss';

const UnresponsiveMenu = ({ menuItems }) => {
  return (
    <div
      className={styles.menu}
    >
      {
        menuItems.map(({ label, componentToShow }) => (
          <PlainButton
            key={label}
            label={label}
            onClick={() => window.location.href = `#${componentToShow}`}
          />
        ))
      }
    </div>
  );
};

UnresponsiveMenu.propTypes = {
  menuItems: PropTypes.array,
};

UnresponsiveMenu.defaultPropTypes = {
  menuItems: [],
};

export default UnresponsiveMenu;