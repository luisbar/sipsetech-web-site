import React from 'react';
import PropTypes from 'prop-types';
import { FaLinkedin, FaTwitterSquare, FaFacebookSquare, FaWhatsappSquare } from 'react-icons/fa';

import styles from '../../styles/molecules/homeFooter.module.scss';

const HomeFooter = ({ socialNetworks, mail }) => {
  return (
    <>
      <div
        className={styles.iconsContainer}
      >
        {
          socialNetworks[0] &&
          <FaTwitterSquare
            key={socialNetworks[0]}
            className={styles.iconMargin}
            onClick={() => window.location.href = socialNetworks[0]}
          />
        }
        {
          socialNetworks[1] &&
          <FaLinkedin
            key={socialNetworks[1]}
            className={styles.iconMargin}
            onClick={() => window.location.href = socialNetworks[1]}
          />
        }
        {
          socialNetworks[2] &&
          <FaFacebookSquare
            key={socialNetworks[2]}
            className={styles.iconMargin}
            onClick={() => window.location.href = socialNetworks[2]}
          />
        }
        {
          socialNetworks[3] &&
          <FaWhatsappSquare
            key={socialNetworks[3]}
            className={styles.icon}
            onClick={() => window.location.href = socialNetworks[3]}
          />
        }
      </div>
      <span
        className={styles.mail}
        onClick={() => window.location.href = `mailto:${mail}`}
      >
        {mail}
      </span>
    </>
  );
};

HomeFooter.propTypes = {
  socialNetworks: PropTypes.array,
  mail: PropTypes.string,
};

HomeFooter.defaultProps = {
  socialNetworks: [],
  mail: 'example@example.com'
};

export default HomeFooter;
