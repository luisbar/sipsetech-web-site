import React from 'react';
import PropTypes from 'prop-types';

import PlainButton from '../atoms/PlainButton';
import styles from '../../styles/molecules/responsiveMenu.module.scss';

const ResponsiveMenu = ({ menuItems, visibility, onMenuItemClicked }) => {

  const onClickMenuItem = (componentToShow) => () => {
    window.location.href = `#${componentToShow}`;
    onMenuItemClicked();
  }

  return (
    <div
      className={visibility ? styles.menuOpened : styles.menuClosed}
    >
      {
        menuItems.map(({ label, componentToShow }) => (
          <div
            key={label}
            className={styles.menuItemContainer}
          >
            <PlainButton
              label={label}
              onClick={onClickMenuItem(componentToShow)}
            />
          </div>
        ))
      }
    </div>
  );
};

ResponsiveMenu.propTypes = {
  menuItems: PropTypes.array,
  visibility: PropTypes.bool,
  onMenuItemClicked: PropTypes.func,
};

ResponsiveMenu.defaultPropTypes = {
  menuItems: [],
  visibility: false,
  onMenuItemClicked: () => {},
};

export default ResponsiveMenu;