import React from 'react';
import PropTypes from 'prop-types';

import Input from '../atoms/Input';
import Button from '../atoms/Button';
import styles from '../../styles/molecules/form.module.scss';

const Form = ({ inputs }) => {

  const onClick = (event) => {
    event.submit();
  }

  return (
    <div
      className={styles.container}
    >
      <form
        method='POST'
        action='/sendEmail'
      >
        {
          inputs.map(({ id, label, type }) => {
            return (
              <Input
                key={id}
                id={id}
                name={id}
                label={label}
                type={type}
              />
            );
          })
        }
        <div
          className={styles.buttonContainer}
        >
          <Button
            label='Enviar'
            onClick={onClick}
          />
        </div>
      </form>
    </div>
  );
};

Form.propTypes = {
  inputs: PropTypes.array,
};

Form.defaultProps = {
  inputs: [],
};

export default Form;