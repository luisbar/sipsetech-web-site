import React from 'react';

import RoundedImage from '../atoms/RoundedImage';
import Subtitle from '../atoms/Subtitle';
import styles from '../../styles/molecules/member.module.scss';

const Member = ({ fluidImage, fixedImage, title, description }) => {
  return (
    <div
      className={styles.container}
    >
      <RoundedImage
        fluid={fluidImage}
        fixed={fixedImage}
      />
      <div
        className={styles.subtitle}
      >
        <Subtitle
          text={title}
        />
      </div>
      <span
        className={styles.description}
      >
        {description}
      </span>
    </div>
  );
};

export default Member;