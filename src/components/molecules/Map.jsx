import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../styles/molecules/map.module.scss';
import Subtitle from '../atoms/Subtitle';

const Map = ({ address, phone }) => {
  return (
    <div
      className={styles.container}
    >
      <iframe
        className={styles.map}
        src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15191.118862434298!2d-63.176621830224605!3d-17.848958549999985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x93f1e98317ea6699%3A0x13c41eb53bce52e!2sParroquia+San+Francisco+de+As%C3%ADs!5e0!3m2!1ses!2sbo!4v1564625676449!5m2!1ses!2sbo'
        frameBorder='0'
        allowFullScreen>
      </iframe>
      <span
        className={styles.address}
      >
        {address}
      </span>
      <Subtitle
        text={'Teléfono'}
      />
      <span
        className={styles.phone}
      >
        {phone}
      </span>
    </div>
  );
};

Map.propTypes = {
  address: PropTypes.string,
  phone: PropTypes.string,
};

Map.defaultProps = {
  address: 'your address',
  phone: 'your phone',
};

export default Map;