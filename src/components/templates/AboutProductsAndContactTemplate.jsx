import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../styles/templates/aboutAndProductsTemplate.module.scss';

const AboutProductsAndContactTemplate = ({ id, header, main }) => {
  return (
    <div
      id={id}
      className={styles.container}
    >
      <div
        className={styles.header}
      >
        {header}
      </div>
      <div
        className={styles.main}
      >
        {main}
      </div>
    </div>
  );
};

AboutProductsAndContactTemplate.propTypes = {
  id: PropTypes.node.isRequired,
  header: PropTypes.node.isRequired,
  main: PropTypes.node.isRequired,
};

export default AboutProductsAndContactTemplate;