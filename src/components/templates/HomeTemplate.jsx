import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../styles/templates/homeTemplate.module.scss';

const HomeTemplate = ({ id, main, footer }) => {
  return (
    <div
      id={id}
      className={styles.container}
    >
      <div
        className={styles.main}
      >
        {main}
      </div>
      <div
        className={styles.footer}
      >
        {footer}
      </div>
    </div>
  );
};

HomeTemplate.propTypes = {
  main: PropTypes.node,
  footer: PropTypes.node,
};

HomeTemplate.defaultProps = {
  main: <div>Slogan</div>,
  footer: <><span>Social Networks</span><span>Emails</span></>,
};

export default HomeTemplate;