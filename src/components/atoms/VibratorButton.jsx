import React from 'react';

import Button from './Button';
import styles from '../../styles/atoms/vibratorButton.module.scss';

const VibratorButton = ({ label, onClick }) => {
  return (
    <div
      className={styles.button}
    >
      <Button
        label={label}
        onClick={onClick}
      />
    </div>
  );
};

export default VibratorButton;