import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../styles/atoms/plainButton.module.scss';

const PlainButton = ({ label, onClick }) => {
  return (
    <span
      className={styles.button}
      onClick={onClick}
    >
      {label}
    </span>
  );
}

PlainButton.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

PlainButton.defaultProps = {
  label: 'Click me!'
};

export default PlainButton;