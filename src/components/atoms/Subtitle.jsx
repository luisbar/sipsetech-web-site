import React from 'react';
import PropTypes from 'prop-types';

const Subtitle = ({ text }) => {
  return (
    <h4>
      {text}
    </h4>
  );
};

Subtitle.propTypes = {
  text: PropTypes.string,
};

Subtitle.defaultProps = {
  text: 'Subtitle',
};

export default Subtitle;