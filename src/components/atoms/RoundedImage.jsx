import React from 'react';
import Img from 'gatsby-image';
import PropTypes from 'prop-types';

import styles from '../../styles/atoms/roundedImage.module.scss';

const RoundedImage = ({ fluid, fixed }) => {
  if (fluid)
    return (
      <Img
        className={styles.img}
        fluid={fluid}
      />
    );
  else
    return (
      <Img
        className={styles.img}
        fixed={fixed}
      />
    );
};

RoundedImage.propTypes = {
  fluid: PropTypes.object,
  fixed: PropTypes.object,
};

export default RoundedImage;
