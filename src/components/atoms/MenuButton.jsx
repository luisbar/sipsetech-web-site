import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../styles/atoms/menuButton.module.scss';

const MenuButton = ({ onMenuButtonClicked, buttonWasPressed }) => {

  return (
    <button
      className={buttonWasPressed ? styles.buttonPressed : styles.button}
      onClick={onMenuButtonClicked}>
      <span className={styles.line}></span>
      <span className={styles.line}></span>
      <span className={styles.line}></span>
    </button>
  );
};

MenuButton.propTypes = {
  onMenuButtonClicked: PropTypes.func,
  buttonWasPressed: PropTypes.bool,
};

MenuButton.defaultProps = {
  onMenuButtonClicked: () => {},
  buttonWasPressed: false,
};

export default MenuButton;