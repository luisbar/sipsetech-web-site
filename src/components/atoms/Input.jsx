import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../styles/atoms/input.module.scss';

const Input = ({ id, label, type }) => {

  const getInput = () => {
    if (type === 'text')
      return (
        <input
          className={styles.input}
          id={id}
          name={id}
        />
      );

    return (
      <textarea
        className={styles.input}
        id={id}
        name={id}
        rows='6'
      />
    );
  }

  return (
    <div
      className={styles.container}
    >
      <label
        className={styles.label}
        htmlFor={id}
      >
        {label}
      </label>
      {getInput()}
    </div>
  );
};

Input.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  type: PropTypes.string,
};

Input.defaultProps = {
  label: 'Label',
  type: 'text',
};

export default Input;